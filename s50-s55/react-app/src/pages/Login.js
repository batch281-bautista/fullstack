import { Button, Form, Row, Col } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import {Navigate, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext';

// sweetalert2 is a simple and useful package for generating user alerts with react js
import Swal2 from 'sweetalert2';

export default function Login(){

	const navigate = useNavigate();

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const [isHiddenEmail, setIsHiddenEmail] = useState(true);
	const [isHiddenPassword, setIsHiddenPassword] = useState(true);
	const [isDisabled, setIsDisabled] = useState(false);

	//Allows us to consume the UserContext object and it's properties to use for user validation
    const { user, setUser} = useContext(UserContext);

	// const [user, setUser] = useState(localStorage.getItem('email'));

	useEffect(()=> {
		if(email){
			setIsHiddenEmail(true);
		} else {
			setIsHiddenEmail(false);
		}
	});

	useEffect(()=> {
		if(password){
			setIsHiddenPassword(true);
		} else {
			setIsHiddenPassword(false);
		}
	});

	useEffect(()=> {
		if(email !== '' && password !== ''){
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}
	}, [email, password]);

	function loginUser(event) {
		event.preventDefault();
		// console.log('hi')

		// Set the email of the authenticated user in the local storage
		// Syntax:
			// localStorage.setItem('property', value)
			// The localStorage.setItem() allows us to manipulate the browser's localStorage property to store information indefinitely to help demonstrate conditional rendering.
			// Because REACT JS is a single page application, using the localStorage will not trigger rerendering of component.
		// localStorage.setItem('email', email);
		// setUser(localStorage.getItem('email'));
		// alert('You are now logged in');
		// navigate('/courses');
		// setEmail('');
		// setPassword('');

		// process fetch request to the corresponding backend API
		// Syntax:
			// fetch('url', {options}).then(response => response.json()).then(data => {});


		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}).then(response =>{
		return(response.json())}).then(data => {
			// console.log(data);
			// if statement to check whether the login is successful.
			if(data === false){
				// console.log('hello')
			// in adding sweetalert2 you have to use .fire method
			Swal2.fire({
				title: "Login unsuccessfully!",
				icon: 'error',
				text: 'Check your login credentials and try again'
			})
			}else{
				// console.log('yo')
			localStorage.setItem('token', data.access)
			retrieveUserDetails(data.access);
			Swal2.fire({
				title: "Login successfully!",
				icon: 'success',
				text: 'Welcome to Zuitt!'
			});
			navigate('/courses');
			}
		})

	};

	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(response => response.json())
		.then(data=> {
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
			console.log(data)
		})
	}

	return (
	user.id === null || user.id === undefined
	?
	<Row>
		<Col className="col-6 mx-auto">
			<h1 className="mt-3 text-center">Login</h1>
			<Form onSubmit={event=> loginUser(event)}>
			      <Form.Group className="mb-3" controlId="formBasicEmail">
			        <Form.Label>Email address</Form.Label>
			        <Form.Control type="email" placeholder="Enter email" value={email} onChange={event=> setEmail(event.target.value)}/>
			        <Form.Text className="text-danger" hidden={isHiddenEmail}>
			          Please input valid Email
			        </Form.Text>
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="formBasicPassword">
			        <Form.Label>Password</Form.Label>
			        <Form.Control type="password" placeholder="Password" value={password} onChange={event=> setPassword(event.target.value)}/>
			        <Form.Text className="text-danger" hidden={isHiddenPassword}>
			        Please input your Password
			      </Form.Text>
			      </Form.Group>
			      
			      
			      <Button variant="success" id="submitBtn" type="submit" disabled={isDisabled}>
			        Login
			      </Button>
			    </Form>
		</Col>
	</Row>
  	:
	<Navigate to= '/'/>
	)
};